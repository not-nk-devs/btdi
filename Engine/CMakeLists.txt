set(PROJECT_NAME BTDI_ENGINE)

add_library(${PROJECT_NAME} STATIC "")

target_sources(${PROJECT_NAME}
PUBLIC
    include/bte/core.hpp

    PRIVATE
    src/dummy.cpp
)

target_include_directories(${PROJECT_NAME}
PUBLIC include/
PRIVATE src/
)