#############################################################################
# Purpose: A re-usable logging system to allow enabling/disabling of verbose
# logs and errors.
#

include(Cmake/Options.cmake)

message(STATUS "${PROJECT_NAME}: Setting up logging system")

# CMake Log
macro(CM_LOG Message)
	if(${PROJECT_NAME}_CMAKE_VERBOSE)
		message(STATUS "${PROJECT_NAME}: ${Message}")
	endif()
endmacro()

# Cmake Error

macro(CM_ERROR Message)
	message(FATAL_ERROR "${PROJECT_NAME}: ${Message}")
endmacro()