######################################################################
# Purpose: Cmake code for downloading and including external projects
# Current deps: SDL2, Vulkan.
#
# Currently tested to be working on:
# - Windows 10 x64  - Ninja + Clang 9.0.0 - Visual Studio 2019
#
#####################################################################
############################   WARNING   ############################
#####################################################################
#
# Vulkan - Requires the vulkan SDK to be installed on your machine
# Latest vulkan SDK can be found here:
# https://vulkan.lunarg.com/sdk/home 
# The installer should automatically add VK_SDK_PATH and VULKAN_SDK
# to the environment variables, if it does not, cmake requires them
# to find vulkan. 
#
##########################   END WARNING   ##########################
#
# Instead of having a million ${PROJECT_NAME} macros around, I decided to
# instead go for using CM (Cmake acronym), doesnt make as much sense
# but its still re-usable.
#

include(Cmake/Options.cmake)
include(Cmake/Logging.cmake)

CM_LOG("Checking & Downloading dependencies")

# Include ExternalProject_Add and find_vulkan
include(ExternalProject)
include(FindVulkan)

set(CM_DEPS_DIR "${CMAKE_SOURCE_DIR}/Extern")

# Set up directory structure

set(CM_TMP_DIR "${CM_DEPS_DIR}/tmp")
set(CM_STAMP_DIR "${CM_DEPS_DIR}/log")
set(CM_DOWNLOAD_DIR "${CM_DEPS_DIR}/download")
set(CM_SOURCE_DIR "${CM_DEPS_DIR}/src")
set(CM_BINARY_DIR "${CM_DEPS_DIR}/bin")

CM_LOG("Directory is at ${CM_DEPS_DIR}")
CM_LOG("Creating folders...")

file(MAKE_DIRECTORY ${CM_DEPS_DIR})
file(MAKE_DIRECTORY ${CM_TMP_DIR})
file(MAKE_DIRECTORY ${CM_STAMP_DIR})
file(MAKE_DIRECTORY ${CM_DOWNLOAD_DIR})
file(MAKE_DIRECTORY ${CM_SOURCE_DIR})
file(MAKE_DIRECTORY ${CM_BINARY_DIR})

### SDL2 ###

set(SDL2_VER "2.0.10")

CM_LOG("Adding dep SDL2 ${SDL2_VER}!")

set(CM_DEPS_SDLNAME "SDL2")

# TODO: For linux, needs to download tar.gz instead of .zip
ExternalProject_Add("${CM_DEPS_SDLNAME}"
    PREFIX "${CM_DEPS_DIR}/${CM_DEPS_SDLNAME}"
    TMP_DIR "${CM_TMP_DIR}/${CM_DEPS_SDLNAME}"
    STAMP_DIR "${CM_STAMP_DIR}/${CM_DEPS_SDLNAME}"
    DOWNLOAD_DIR "${CM_DOWNLOAD_DIR}/${CM_DEPS_SDLNAME}"
    SOURCE_DIR "${CM_SOURCE_DIR}/${CM_DEPS_SDLNAME}"
    BINARY_DIR "${CM_BINARY_DIR}/${CM_DEPS_SDLNAME}"

    URL http://www.libsdl.org/release/SDL2-${SDL2_VER}.zip

    INSTALL_COMMAND ""
)

### Vulkan ###
CM_LOG("Checking if Vulkan is installed!")

find_package(Vulkan REQUIRED)

if(Vulkan_FOUND)
    CM_LOG("Found vulkan!")
else()
    CM_ERROR("Vulkan not found!")
endif()

# Dummy program for downloading deps

add_executable(DepExecutable "Cmake/dummy.cpp")

add_dependencies(DepExecutable 
SDL2
)