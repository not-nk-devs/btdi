################################################################
# Purpose: Options for the compiler and the other cmake modules.
#

set(PROJECT_NAME "BTDI")

message(STATUS "${PROJECT_NAME}: Setting options for ${PROJECT_NAME}")

option(${PROJECT_NAME}_CMAKE_VERBOSE 
"Vebose messages, turn on for debugging Cmake issues." 
ON)

# TODO: Update this to C++20 when it released
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED true)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
